﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardBack : MonoBehaviour
{
    public GameObject cardBack;
    public GameObject cardFront;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ThisCard.staticCardBack == true)
        {
            cardBack.SetActive(true);
            cardFront.SetActive(false);
        }
        else
        {
            cardBack.SetActive(false);
            cardFront.SetActive(true);
        }
    }
}
