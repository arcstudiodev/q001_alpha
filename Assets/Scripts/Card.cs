﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]

public class Card 
{
    public int id;
    public string cardName;
    public int type;
    public string cardDescription;

    public Sprite typeImage;
    public Sprite thisImage;

    public string color;

    public Card()
    {

    }

    public Card(int Id, string CardName, int Type, Sprite TypeImage, string CardDescription, Sprite ThisImage, string Color)
    {
        id = Id;
        cardName = CardName;
        type = Type;
        cardDescription = CardDescription;

        typeImage = TypeImage;
        thisImage = ThisImage;

        color = Color;
    }
}


