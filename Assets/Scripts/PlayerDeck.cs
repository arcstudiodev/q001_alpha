﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeck : MonoBehaviour
{

    public List<Card> deck = new List<Card>();
    public List<Card> container = new List<Card>();

    public int x;
    public int deckSize;

    public GameObject cardinDeck1;
    public GameObject cardinDeck2;
    public GameObject cardinDeck3;
    public GameObject cardinDeck4;

    public GameObject CardBack;
    public GameObject Deck;

    public GameObject[] Clones;


    // Start is called before the first frame update
    void Start()
    {
        x = 0;
        deckSize = 12;

        for (int i = 0; i < deckSize; i++)
        {
            x = Random.Range(1, 5);
            deck[i] = CardDatabase.cardList[x];

        }


    }

    // Update is called once per frame
    void Update()
    {
        if (deckSize < 10)
        {
            cardinDeck1.SetActive(false);
        }
        if (deckSize < 6)
        {
            cardinDeck2.SetActive(false);
        }
        if (deckSize < 2)
        {
            cardinDeck3.SetActive(false);
        }
        if (deckSize < 1)
        {
            cardinDeck4.SetActive(false);
        }


    }

    IEnumerator Example()
    {
        yield return new WaitForSeconds(1);
        Clones = GameObject.FindGameObjectsWithTag("Clone");
    }

    public void Shuffle()
    {
        for (int i = 0; i < deckSize; i++)
        {
            container[0] = deck[i];
            int randomIndex = Random.Range(i, deckSize);
            deck[i] = deck[randomIndex];
            deck[randomIndex] = container[0];
            
        }
        Instantiate(CardBack, transform.position, transform.rotation);
        StartCoroutine(Example());
    }
}
