﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDatabase : MonoBehaviour
{
    public static List<Card> cardList = new List<Card> ();

    void Awake() 
    {
        cardList.Add(new Card(0, "None", 0, Resources.Load<Sprite>("Q"), "None", Resources.Load <Sprite>("Q"), "None"));
        cardList.Add(new Card(1, "Evil Ryu", 1, Resources.Load<Sprite>("rock_ico"), "Double Damage", Resources.Load<Sprite>("ERYU"), "Red"));
        cardList.Add(new Card(2, "Skull Dandy", 2, Resources.Load<Sprite>("paper_ico"),"None", Resources.Load<Sprite>("SKLD"), "Blue"));
        cardList.Add(new Card(3, "Kurtis", 3, Resources.Load<Sprite>("sciss_ico"), "None", Resources.Load<Sprite>("KRTS"), "Yellow"));
        cardList.Add(new Card(4, "Sand Queen", 3, Resources.Load<Sprite>("sciss_ico"), "None", Resources.Load<Sprite>("CLAW"),"Purple"));
    }

}
