﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ThisCard : MonoBehaviour
{
    public List<Card> thisCard = new List<Card>();
    public int thisId;

    public int id;
    public string cardName;
    public int type;
    public string cardDescription;

    public Text nameText;
    public Text typeText;
    public Text descriptionText;

    public Sprite thisSprite;
    public Image thatImage;

    public Sprite typeSprite;
    public Image typeCard;

    public Image frame;

    public bool cardBack;
    public static bool staticCardBack;

    void Start()
    {
        thisCard [0] = CardDatabase.cardList[thisId];
    }

    // Update is called once per frame
    void Update()
    {
        id =thisCard[0].id;
        cardName =thisCard[0].cardName;
        type =thisCard[0].type;
        cardDescription =thisCard[0].cardDescription;

        thisSprite = thisCard[0].thisImage;
        typeSprite = thisCard[0].typeImage;

        nameText.text = "" + cardName;
        typeText.text = "" + type;
        descriptionText.text = "" + cardDescription;

        thatImage.sprite = thisSprite;
        typeCard.sprite = typeSprite;

        if(thisCard[0].color=="Red")
        {
            frame.GetComponent<Image>().color = new Color32(255, 0, 0, 255);
        }

        if (thisCard[0].color == "Blue")
        {
            frame.GetComponent<Image>().color = new Color32(0, 0, 255, 255);
        }

        if (thisCard[0].color == "Yellow")
        {
            frame.GetComponent<Image>().color = new Color32(255, 255, 0, 255);
        }

        if (thisCard[0].color == "Purple")
        {
            frame.GetComponent<Image>().color = new Color32(255, 0, 255, 255);
        }

        staticCardBack = cardBack;

    }
}
